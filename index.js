 // console.log('Hello World')

/*GET method that will retrieve all the
to do list items from JSON Placeholder API.*/

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => console.log(data))



/*GET method that will retrieve a
single to do list item from JSON Placeholder API.*/

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((data) => console.log(data))


/*print a message in the console that will
provide the title and status*/

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then ((response) => response.json())
.then ((data) => console.log(data.title+" " +data.completed))

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => {
	console.log(response.status)
})



/*POST method that will create a to
do list item using the JSON Placeholder API.*/

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST', 
	headers: {
		'Content-Type': 'application/json'},
		body:JSON.stringify({
			title: 'Created To Do List Item', 
			userId: 1,
			completed: false
		})
	})
.then((response)=> response.json())
.then((data)=> console.log(data))


/*PUT method that will update a to do
list item using the JSON Placeholder API.*/

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT', 
	headers: {
		'Content-Type': 'application/json'},
		body:JSON.stringify({
			title: 'Updated To-Do-List Item', 
			description:" To update the my to do list with a different data structure",
			status: "Pending",
			dateCompleted: "Pending", 
			userId: 1,

		})	
			})	
.then((response)=> response.json())
.then((data)=> console.log(data))




/*PATCH method that will update a to
do list item using the JSON Placeholder API.*/
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH', 
	headers: {
		'Content-Type': 'application/json'},
		body:JSON.stringify({
		title: 'delectus aut autem', 
			status: "Complete",
			dateCompleted: "07/09/21", 
			userId: 1,
	
		})
	})
.then((response)=> response.json())
.then((data)=> console.log(data))



//[Deleting a Post]
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
})

//[Deleting a Post END]





/*STRETCH GOAL: create an array using the map method to
return just the title of every item and print the result in the console.*/

/*const map = Array.prototype.map;*/


/*fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => {
     let list = data.map((todo_item) => {
    return todo_item.title
  })
console.log(list)
})*/

